package com.example.finalproject;

public class Menu {
	private int id;
	private String date;
	private String menu;
	
	public  Menu(String date, String menu){
		super();
		this.date=date;
		this.menu=menu;
	}
	
	
	
	public String getDate() {
		return date;
	}



	public void setDate(String date) {
		this.date = date;
	}



	public String getMenu() {
		return menu;
	}



	public void setMenu(String menu) {
		this.menu = menu;
	}



	public String toString(){
		return "Menu [id="+id +",date ="+date+",menu="+menu+"]";
	}
}

