package com.example.test;

import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

//import com.example.finalproject.ServiceHandler;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

public class MainActivity extends ActionBarActivity {

	public class Person{
		public String name;
		public int age;
	}
	
	String tag="test";
	private static final String DATABASE_NAME="db";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);  
       new UpdateCalendar().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class UpdateCalendar extends AsyncTask<Void, Void, Void> {

		private String url = "http://www.google.com/calendar/feeds/gqccak2junkb7eup9ls76k919c@group.calendar.google.com/public/full?alt=json&orderby=starttime&max-results=15&singleevents=true&sortorder=ascending&futureevents=true";

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			ServiceHandler sh = new ServiceHandler();
			String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);
			Log.d("Response: ", "> " + jsonStr);
			if (jsonStr != null) {
				try {
					JSONObject jsonObj = new JSONObject(jsonStr);
					JSONObject feed = jsonObj.getJSONObject("feed");
					JSONArray entry = feed.getJSONArray("entry");
					int i = 0;
					for (; i < entry.length(); i++) {
						JSONObject e=entry.getJSONObject(i);
						JSONObject when=e.getJSONArray("gd$when").getJSONObject(0);
						String startTime=when.getString("startTime");
						String endTime=when.getString("endTime");
						JSONObject title=e.getJSONObject("title");
						String t=title.getString("$t");
						if(t.contains("1."))
						{
							
							String[] sss=t.split("(or\\s)?([1-3])\\.");
//							db.addMenu("startTime", sss[1]);
//							db.addMenu("startTime", sss[2]);
//							db.addMenu("startTime", sss[3]);
						}
					}

				} catch (Exception e) {
					Log.d("JSON",e.toString());
				} finally {
				}
			}
			else{
				Log.e("ServiceHandler", "Couldn't get any data from the url");
			}
			return null;
		}

	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			Calendar calendar = Calendar.getInstance();
			System.out.println("dom: "+calendar.get(Calendar.DAY_OF_MONTH));
			System.out.println("dow: "+calendar.get(Calendar.DAY_OF_WEEK));
			System.out.println("New Calendar:= " + calendar.getTime().toString());
			return rootView;
		}
	}

}
