package com.example.test;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME="db";
	private static final String DATE = "date";
	private static final String MENU = "menu";

	public DatabaseHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

		db.execSQL("CREATE TABLE calendar(_id INTEGER PRIMARY KEY AUTOINCREMENT,date TEXT,menu TEXT);");	
		ContentValues cv=new ContentValues();	
		cv.put(DATE, "2014-06-02");
		cv.put(MENU,"Popcorn Chicken w/Soft Roll ");
		db.insert("calendar",null, cv);
		cv.put(DATE, "2014-06-02");
		cv.put(MENU,"Grilled Cheese");
		db.insert("calendar",null, cv);
		cv.put(DATE, "2014-06-02");
		cv.put(MENU, "Minnie Mouse Salad w/Dinner Roll Sweet Potato Fries Broccoli, Orange Smiles or Fresh Apple");
		db.insert("calendar",null, cv);
		cv.put(DATE, "2014-06-03");
		cv.put(MENU,"Turkey Hot Dog on a Bun" );
		db.insert("calendar",null, cv);
		cv.put(DATE,"2014-06-03");
		cv.put(MENU, "Grilled Cheese");
		db.insert("calendar",null, cv);
		cv.put(DATE, "2014-06-03");
		cv.put(MENU, "Garden Tossed Saladw/Egg & Roll Corn Niblets, Lunch Bunch Grapes or “NEW” Blue Raspberry Frozen Fruit Pushup");
		db.insert("calendar",null, cv);
		cv.put(DATE, "2014-06-04");
		cv.put(MENU, "Cheese or Pepperoni Pizza w/Greens ");
		db.insert("calendar",null, cv);
		cv.put(DATE, "2014-06-04");
		cv.put(MENU,"Grilled Cheese Sandwich ");
		db.insert("calendar", null, cv);
		cv.put(DATE, "2014-06-04");
		cv.put(MENU, "Poppin Chicken Salad w/Soft RollBroccoli, Celery SticksPineapple, Apple Slices or Orange Smiles");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-05");
		cv.put(MENU,"Nachos w/Beef& Cheese or Beans");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-05");
		cv.put(MENU,"Grilled Cheese Sandwich ");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-05");
		cv.put(MENU,"Garden Tossed Salad w/Egg &RollRefried Beans,Cucumber Slices, Orange Smiles,Pears or Grapes");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-06");
		cv.put(MENU,"Chef’s Inspiration");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-09");
		cv.put(MENU,"Chicken Nuggets w/Soft Roll");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-09");
		cv.put(MENU,"Grilled Cheese ");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-09");
		cv.put(MENU,"Minnie Mouse Salad w/Soft Dinner RollMashed Potatoes w/ Chicken Gravy, Orange Smiles,Apple or Pineapple Chunks");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-10");
		cv.put(MENU,"Chef’s Inspiration“NEW” Blue Raspberry Frozen Fruit Pushup");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-11");
		cv.put(MENU,"Cheese or Pepperoni Pizza w/Greens");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-11");
		cv.put(MENU,"Grilled Cheese Sandwich ");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-11");
		cv.put(MENU,"Poppin Chicken Salad w/Soft RollCrunchy Carrots, Celery Sticks, Broccoli, Apple Slices or Peaches");
		db.insert("calendar", null, cv);
		cv.put(DATE,"2014-06-12");
		cv.put(MENU,"½ Day of School");
		db.insert("calendar", null, cv);
		
		
		//cv.put(MENU, value);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
